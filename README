Esse repo contem o resultado do meu Trabalho de conclusão de curso de de Pós-graduação Lato Sensu
em Inteligência Artificial e Aprendizado de Máquina na puc minas.

Teve como base a seguinte competição do kaggle:
https://www.kaggle.com/c/cassava-leaf-disease-classification/overview

Contém os modelos finais, métricas de qualidade deles e os notebooks criados.

Lista de notebooks:

1) notebooks/cassava-leaf-models-1-3.ipynb
    Notebook base de treinamento dos modelos

2) notebook/cassava-leaf-models-metrics.ipynb
    Notebook simples para calcular o valor médio das métricas para os modelos
    obtidos com menor valor de perda nos datasets de validação via cross-validation.

3) notebooks/cassava-models-submission.ipynb
    Notebook básico usado nas submissões contendo dois modos de ensemble entre os
    modelos obtidos via cross-validation (hard-voting e soft-voting)

Esses notebooks usam como modelos básicos de partida os modelos efficientnet-b0 e efficientne-b5
no formato de feature-vector que podem ser obtidos nos seguintes links do tensorflow-hub:

https://tfhub.dev/tensorflow/efficientnet/b0/feature-vector/1
https://tfhub.dev/tensorflow/efficientnet/b5/feature-vector/1

Uma copia deles foi adicionado na pasta modelos_base ( a licensa deles é a apache 2.0 como pode
ser visto nessa issue no github do tensorflow-hub: https://github.com/tensorflow/hub/issues/12 )

No kaggle esses modelos efficientnet foram adicionados como datasets privados devido a não haver,
no momento que iniciei o trabalho, implementação desses modelos base na biblioteca tf.keras.application
(somente quando já estava para entregar o trabalho o kaggle disponibilizou o tensorflow 2.4 com tpus)

Os modelos finais treinados por mim estão disponiveis como um conjunto de pesos salvos em numpy.
Esse formato foi usado por que quando do início desse trabalho a versão do tensorflow no kaggle
para tpus era a 2.2 como já dito anteriormente, que não permitia salvar localmente modelos no
formato padrão do tensorflow quando usando tpu, sendo que optei por um workaround usando pickle.
Para carregar os modelos é preciso instanciar um modelo usando as funções disponiveis nos notebooks
(que chamam os modelos base efficientnet anteriores) e setar os pesos carregados dos arquivos pickle.
Exemplos de carregamento dos modelos podem ser vistos nos notebooks.

Esse repo usa git lfs para fornecer os arquivos .pickle dos modelos, visto serem de tamanho considerável
https://docs.gitlab.com/ee/topics/git/lfs/
